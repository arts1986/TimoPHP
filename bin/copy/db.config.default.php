<?php
return [
    'mysql' => [
        'master' => [
            'dsn' => 'mysql:host=localhost;port=3306;dbname=com_blog;charset=utf8',
            'user' => 'root',
            'password' => 123456,
            'prefix' => 'com_',
        ]
    ],
];